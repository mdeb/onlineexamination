<?php

class Users_Form_Register extends Zend_Form {

  public function init() {
    /* Form Elements & Other Definitions Here ... */
    // initialize form
    $this->setAction('Users/login/registration')
        ->setMethod('POST');

// create text input for name
    $username = new Zend_Form_Element_Text('username');
    $username->removeDecorator('HtmlTag')
		->removeDecorator('Errors')
		->setDecorators(FormDecorators::$simpleElementDecorators)
        ->setOptions(array('size' => '15', 'class' => 'form-text required'))
        ->setRequired(true)
        ->addValidator('Alnum')
        ->addFilter('HtmlEntities')
        ->addFilter('StringTrim');

// create text input for password
    $password = new Zend_Form_Element_Password('password');
    $password->removeDecorator('HtmlTag')
	->setDecorators(FormDecorators::$simpleElementDecorators)
        ->setOptions(array('size' => '15', 'class' => 'form-text required'))
        ->setRequired(true)
        ->addFilter('HtmlEntities')
        ->addFilter('StringTrim');

//confirm password
    $cpassword = new Zend_Form_Element_Password('cpassword');
    $cpassword->removeDecorator('HtmlTag')
	->setDecorators(FormDecorators::$simpleElementDecorators)
        ->setOptions(array('size' => '15', 'class' => 'form-text required'))
        ->setRequired(true)
        ->addFilter('HtmlEntities')
        ->addFilter('StringTrim')
		->addValidator('Identical', false, array('token' => 'password',
		'messages' => array(
			Zend_Validate_Identical::NOT_SAME 
				=> "Password doesn't match",
			Zend_Validate_Identical::MISSING_TOKEN
				=> "Please retype password")
		));

	$email = new Zend_Form_Element_Text('email');
    $email->setDecorators(FormDecorators::$simpleElementDecorators)
		->setOptions(array('size' => '16'))
		->setRequired(true)
		->addValidator('EmailAddress', true, array(
		'messages' => array(
			Zend_Validate_EmailAddress::INVALID
				=> "ERROR: Invalid email address",
			Zend_Validate_EmailAddress::INVALID_FORMAT
				=> "Invalid email address format")
));
// create captcha
$captcha = new Zend_Form_Element_Captcha('captcha', array(
'captcha' => array(
'captcha' => 'Figlet',
'wordLen' => 5,
'timeout' => 300,
)
));
$captcha->setLabel('Verification:');

// create submit button
    $submit = new Zend_Form_Element_Submit('submit');
    $submit->setLabel('Register')
        ->setOptions(array('class' => 'submit form-submit'))->setDecorators(array('ViewHelper'));

// attach elements to form
    $this->addElement($username)
        ->addElement($password)
		->addElement($cpassword)
		->addElement($email)
				->addElement($captcha)
        ->addElement($submit);
  }

}
 class FormDecorators {
    public static $simpleElementDecorators = array(
        array('ViewHelper'),
        array('Label', array('tag' => 'span', 'escape' => false, 'requiredPrefix' => '<span class="required">* </span>')),
        array('Description', array('tag' => 'div', 'class' => 'desc-item')),
        array('Errors', array('tag' => 'span', 'class' => 'errors')),
        array('HtmlTag', array('tag' => 'div', 'class' => 'form-item'))
    );
    }
