<?php

class OE_Install {

public static function run() {

	$maxExecutionTime = 300;

	error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
	@ini_set("max_execution_time", $maxExecutionTime);
    set_time_limit($maxExecutionTime);
    mb_internal_encoding("UTF-8");
	
    //set default time set zone 	
	$defaultTimezone = @date_default_timezone_get();
    if(!$defaultTimezone) {
		date_default_timezone_set("UTC"); // UTC -> default timezone
    }

    // check some system variables
    if (version_compare(PHP_VERSION, '5.3.0', "<")) {
        $m = "Your site - Online Examination requires at least PHP version 5.3.0 your PHP version is: " . PHP_VERSION;
        OE_Tool::exitWithError($m);
        }	
	
	$conf = OE_Config::getSystemConfig();	
	 if(!$conf) {
             //redirect to installer if configuration isn't present
            //if (!preg_match("/^\/install.*/", $_SERVER["REQUEST_URI"])) {
              //  header("Location: install/");
                //exit;
            //}
        }
		
}

}