<?php

class OE_Config {
public static function getSystemConfig () {
        $config = null;

            try {
                $config = new Zend_Config_Xml(OE_CONFIGURATION_SYSTEM);
                self::setSystemConfig($config);
            } catch (Exception $e) {
                //OE_Tool::exitWithError("Cannot find system configuration, should be located at: " . OE_CONFIGURATION_SYSTEM);
                if(is_file(OE_CONFIGURATION_SYSTEM)) {
                    $m = "Your system.xml located at " . OE_CONFIGURATION_SYSTEM . " is invalid, please check and correct it manually!";
                    OE_Tool::exitWithError($m);
                }
            }

        return $config;

}

}